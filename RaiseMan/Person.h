//
//  Person.h
//  RaiseMan
//
//  Created by Yihang Ho on 9/22/14.
//  Copyright (c) 2014 Yihang Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef RaiseMan_Person_h
#define RaiseMan_Person_h

@interface Person: NSObject {
    NSString *personName;
    float expectedRaise;
}

@property(readwrite, copy) NSString *personName;
@property(readwrite) float expectedRaise;

@end

#endif
