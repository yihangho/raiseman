//
//  Person.m
//  RaiseMan
//
//  Created by Yihang Ho on 9/22/14.
//  Copyright (c) 2014 Yihang Ho. All rights reserved.
//

#import "Person.h"

@implementation Person

@synthesize personName;
@synthesize expectedRaise;

-(id) init
{
    self = [super init];
    if (self) {
        personName = @"New Person";
        expectedRaise = 0.05;
    }
    return self;
}

@end